<?php

namespace Exemple;

use Exemple\Controller\ContactController;
use Exemple\Controller\IndexController;

return [
    'bjyauthorize'    => [
        'guards'                => [
            'BjyAuthorize\Guard\Controller'         => [
                ['controller' => IndexController::class, 'roles' => 'guest'],
                ['controller' => ContactController::class, 'roles' => 'guest'],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'exemple' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/exemple',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'contact' => [
                        'type'          => 'Literal',
                        'options'       => [
                            'route'    => '/contact',
                            'defaults' => [
                                'controller' => ContactController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'default' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'       => '/:action',
                                    'constraints' => [
                                        /**
                                         * @see ContactController::ajouterAction()
                                         * @see ContactController::ajouterIdentiteAction()
                                         * @see ContactController::ajouterAdresseAction()
                                         * @see ContactController::ajouterMessageAction()
                                         * @see ContactController::ajouterAnnulerAction()
                                         * @see ContactController::ajouterConfirmerAction()
                                         * @see ContactController::ajouterEnregistrerAction()
                                         */
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'exemple' => [
                        'label'    => 'Exemple',
                        'route'    => 'exemple',
                        'icon'     => 'glyphicon glyphicon-cog',
                        'pages' => [
                            'contact' => [
                                'label' => 'Formulaire multipage',
                                'route' => 'exemple/contact/default',
                                'action' => 'ajouter'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [

        ],
    ],

    'translator'      => [
        'locale'                    => 'fr_FR', // en_US
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],

    'controllers'     => [
        'invokables' => [
            IndexController::class => IndexController::class,
            ContactController::class => ContactController::class,
        ],
    ],

    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
