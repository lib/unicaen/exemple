<?php

namespace Exemple\Controller;

use Exemple\Form\ContactMultipageForm;
use UnicaenApp\Controller\Plugin\MultipageFormPlugin;
use UnicaenApp\Form\MultipageForm;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * @method MultipageFormPlugin multipageForm(MultipageForm $form)
 *
 * @author Unicaen
 */
class ContactController extends AbstractActionController
{
    /**
     * @var ContactMultipageForm
     */
    protected $form;

    public function __construct()
    {
        $this->form = new ContactMultipageForm('contact');
        $this->form->setActionPrefix('ajouter-');
    }

    public function ajouterAction()
    {
        return $this->multipageForm($this->form)->start(); // réinit du plugin et redirection vers la 1ère étape
    }

    public function ajouterIdentiteAction()
    {
        return $this->multipageForm($this->form)->process();
    }

    public function ajouterAdresseAction()
    {
        return $this->multipageForm($this->form)->process();
    }

    public function ajouterMessageAction()
    {
        return $this->multipageForm($this->form)->process();
    }

    public function ajouterAnnulerAction()
    {
        return $this->redirect()->toRoute('exemple/contact');
    }

    public function ajouterConfirmerAction()
    {
        $response = $this->multipageForm($this->form)->process();
        if ($response instanceof Response) {
            return $response;
        }
        return array('form' => $this->form);
    }

    public function ajouterEnregistrerAction()
    {
        $data = $this->multipageForm($this->form)->getFormSessionData();
        var_dump($data);
//        return $this->redirect()->toRoute('home');
        return false;
    }
}